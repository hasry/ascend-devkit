<div align=center>
  <img src="doc/images/logo_ascend.png"/>
</div>
<div align=center><h1>从零开始的镜像制作</h1></div>
<div style="font-size:1.1em;margin-bottom:2em;" align="center">基于Ubuntu和OpenEuler制作Atlas200I DK A2镜像</div>
<div style="font-size:1.1em;font-weight:bold" align=center>
  <p>
  <a style="margin-right:1em;" href="https://www.hiascend.com/zh/">昇腾官网</a>
  <a style="margin-right:1em;" href="https://www.hiascend.com/hardware/developer-kit-a2/resource">开发资源</a>
  <a style="margin-right:1em;" href="https://www.hiascend.com/forum/">昇腾论坛</a>
  <a href="https://gitee.com/HUAWEI-ASCEND/ascend-devkit/wikis/A200I%20DK%20A2/Atlas%20200I%20DK%20A2%20%E9%95%9C%E5%83%8F%E8%AE%BE%E8%AE%A1%E6%96%87%E6%A1%A3">镜像文档</a>
  </p>
</div>
<div style="font-size:2.0em" align=center>
  <p>
  <a href="https://gitee.com/HUAWEI-ASCEND/ascend-devkit/pulls"><img src="https://img.shields.io/badge/Pull%20Request-Click%20me-%23293241?style=flat-square&labelColor=%23293241&color=%233D5A80"></a>
  <a href="https://gitee.com/HUAWEI-ASCEND/ascend-devkit/issues"><img src="https://img.shields.io/badge/Issues-Click%20me-%23293241?style=flat-square&labelColor=%23293241&color=%23EE6C4D"></a>
  <a href="https://gitee.com/HUAWEI-ASCEND/ascend-devkit/stargazers"><img src="https://img.shields.io/badge/Stars-Click%20me-%23293241?style=flat-square&labelColor=%23293241&color=%23E0FBFC"></a>
  <a href="https://gitee.com/HUAWEI-ASCEND/ascend-devkit/members"><img src="https://img.shields.io/badge/Members-Click%20me-%23293241?style=flat-square&labelColor=%23293241&color=%2398C1D9"></a>
  </p>
</div>

## 1 介绍

~~你好呀，欢迎来到从零开始的镜像制作生活。~~

本文介绍了以**Ubuntu**或者**OpenEuler**文件系统为始，制作**Atlas 200I DK A2开发板镜像**流程。

需要用户拥有**一定的Linux基础**，并且想要自己制作一个镜像的**决心** (相信我这很重要)。

按照本流程操作后，除了原始系统本身功能和依赖外，镜像中还会存在：

1. Ascend 310B 固件
2. Ascend 310B npu驱动
3. CANN
4. mxVision
5. miniconda (OpenEuler不含) 以及python
6. jupyterlab、protobuf等相关pip依赖
7. aclruntime和ais_bench

## 2 特性

1. 多系统多版本：支持基于Ubuntu和OpenEuler系统制作镜像，用户也可自行添加其他系统脚本；
2. 配置动态修改：用户可以通过修改配置文件来控制制作过程中脚本的行为；
3. 中断异常策略：在中断或者异常退出后再次执行脚本，会以中断或异常退出的函数为开始 (目前无法精确到行) 执行脚本；
4. 可重复执行性：在全部运行完毕后，配置文件将会刷新函数控制相配置项，使用户在无需还原配置文件的条件下可以直接反复运行脚本；
5. 脚本结构清晰：参见结构章节，用户可以自行选择以哪种镜像为最终目的；

## 3 准备

制作Atlas 200I DK A2镜像所需软硬件条件如下：

1. 一台带有USB接口、系统为Linux aarch64、**使用apt命令进行下载** (当前脚本只支持apt命令) 的服务器 (这里服务器是泛指概念，开发板自身都可以，只要满足使用条件)
2. 另一台可以使用Micro SD卡作为运行系统的服务器 (如果上述服务器可以也可以用同一个)
3. 一个读卡器
4. 一张容量至少为32G (保留至少有15G的空间，因为压缩扩容镜像的制作需要12G左右的空间) 的SD卡
5. 请保持网络畅通，**尤其是与GitHub的网络**

## 4 结构

本章节简要介绍脚本整体结构，详细信息请查看文档。

镜像制作分为**最小镜像(minimal)**、**完整镜像(complete)**、**压缩扩容镜像(compress)** 三个模块，介绍如下：

| 模块名称   | 对应路径         | 功能简介               |
|--------|--------------|--------------------|
| 最小镜像   | src/minimal  | 可以在DK上启动但缺少部分依赖的镜像 |
| 完整镜像   | src/complete | 完整依赖镜像             |
| 压缩扩容镜像 | src/compress | 带有压缩扩容功能的完整依赖镜像    |

具体目录结构如下：

```bash
image-builder
|   README.md
|          
\---src
    +---complete
    |   |   base.sh
    |   |   
    |   +---openEuler
    |   |   \---22.03
    |   |           cfg.json
    |   |           func.sh
    |   |           
    |   \---ubuntu
    |       \---22.04
    |               cfg.json
    |               func.sh
    |               
    +---compress
    |   |   base.sh
    |   |   config.ini
    |   |   E2E_samples_download_tool.sh
    |   |   
    |   +---openEuler
    |   |   \---22.03
    |   |           expand.sh
    |   |           
    |   \---ubuntu
    |       \---22.04
    |               expand.sh
    |               
    \---minimal
        |   base.sh
        |   
        +---openEuler
        |   \---22.03
        |           cfg.json
        |           func.sh
        |           
        \---ubuntu
            \---22.04
                    cfg.json
                    func.sh
```

每个模块组成如下：

| 名称              | 功能                                                         |
| ----------------- | ------------------------------------------------------------ |
| base.sh           | 脚本入口，承载与发行版本无关的公共功能                       |
| 发行版名称/版本号 | 目前提供ubuntu和openEuler版本                                |
| cfg.json          | 配置文件，例如依赖下载链接，运行函数执行逻辑等               |
| func.sh           | 函数库，制作镜像核心命令，包括依赖安装，增加服务，修改系统配置文件等 |

**注意：**

1. **用户可以自行添加或修改发行版名称和版本号，只要执行命令时选择正确的路径即可**

2. **配置文件和函数库文件上传到服务器时请注意Windows和Linux换行符的不同，需要LF，而不是CRLF**

3. **用户可以自行添加配置文件的配置，函数库文件中的函数，只要二者能够正确配合即可**
4. **脚本在运行时，如果用户自己暂停或者某个函数异常导致脚本中断，下次执行脚本时将从上次中断的函数开始执行；而当全部正常运行后，再次执行时会从头开始。**

## 5 使用

**如果没有阅读结构章节的话，建议您查看该章节后再阅读本章节。**

### 5.1 最小镜像

请按照如下方法进行最小镜像的制作：

1. 将插有Micro SD卡的读卡器插入到准备好的服务器

2. (可选) 修改配置文件

   您可以选择使用代码仓默认的配置，也可以自行修改配置。如果您对Linux系统的操作不甚熟悉的话，建议在您熟悉的系统上将配置文件修改好后执行下一步骤。

3. 使用scp或者MobaXterm等远程终端工具将`src`下的`minimal`路径上传到服务器任意目录，注意上传格式

4. 在服务器中进入到上传后的`minimal`路径下，可以使用`bash base.sh -h`查看帮助信息

5. 使用`fdisk -l`查看磁盘编号，如`/dev/sdb`

6. (可选) 依赖上传

   如果服务器的网速较慢，或者您已经提前准备好了相关依赖，可以通过步骤4中的方式直接将依赖文件上传到指定目录中。如果您没有指定自己的下载目录，请将依赖全部上传至默认依赖下载目录下。以目标为ubuntu 22.04为例，默认的下载目录为`minimal/ubuntu/22.04/download/`。

7. 使用如下命令进行最小镜像的写卡

   脚本会按照给出的路径寻找配置文件(cfg.json)和运行函数(func.sh)进行写卡操作，以Ubuntu LTS 22.04以及OpenEuler 22.03为例，提供如下写卡命令参考。注意发行版本入参末尾可以没有`/`，脚本中会进行判断。

   - 如果想要制作Ubuntu镜像：

     ```shell
     bash base.sh ubuntu/22.04/ /dev/sdb
     ```

   - 如果想要制作OpenEuler镜像：

     ```shell
     bash base.sh openEuler/22.03/ /dev/sdb
     ```

### 5.2 完整镜像

请按照如下方法进行完整镜像的制作：

1. 将制作好**最小镜像**的Micro SD卡插入Atlas 200I DK A2开发板

2. 按照[官方文档](https://www.hiascend.com/document/detail/zh/Atlas200IDKA2DeveloperKit/23.0.RC1/qs/qs_0018.html)的指导启动、连接开发板，并给开发板共享网络

3. (可选) 修改配置文件

   和最小镜像章节一样，可以修改相应的配置。

4. 使用命令或终端工具将`src`的`complete`路径上传至开发板镜像任意目录，注意上传格式

5. 在服务器中进入到上传后的`complete`路径下，可以使用`bash base.sh -h`查看帮助信息

6. (可选) 依赖上传

   同样，如果您已经准备好所有依赖，或者认为开发板的访问网络速度过慢，可以将准备好的依赖通过步骤4的方式上传至开发板镜像中。

7. 使用如下命令进行完整镜像的制作

   以Ubuntu LTS 22.04为例，注意发行版本入参末尾可以没有`/`，脚本中会进行判断。

   - 基础命令

     ```shell
     bash base.sh ubuntu/22.04/
     ```

   - 如果您想要查看详细的打印信息，则需要参数`v`

     ```shell
     bash base.sh -v ubuntu/22.04/
     ```

   - 如果您想要运行成功后，自动重启以便某些功能或配置生效，则需要参数`r`

     ```shell
     bash base.sh -r ubuntu/22.04/
     ```

   `v`与`r`参数可以结合使用，详情请见文档或使用`bash base.sh -h`查看帮助信息。

注意：

1. MindX SDK mxVision的安装

   脚本只能安装root用户的mxVision。由于mxVision权限策略，HwHiAiUser以及其他用户的mxVision请切换或更换用户登录后自行安装。

2. OpenEuler的root用户登录

   OpenEuler在最小镜像做好后，只能先使用HwHiAiUser用户登录，随后通过以下步骤设置root用户登录及密码：

   1. `su`
   2. `passwd`
   3. 再次输入密码即可生效

### 5.3 压缩扩容镜像

请按照如下方法进行压缩扩容镜像的制作：

1. 将承载**完整镜像**的Micro SD卡插入读卡器后，将读卡器插入到之前准备的服务器上

2. 使用命令或终端工具将`src`的`compress`路径上传至开发板镜像任意目录，注意上传格式

3. 在服务器中进入到上传后的`compress`路径下，可以使用`bash base.sh -h`查看帮助信息

4. 使用`fdisk -l`查看磁盘编号，如`/dev/sdb`

5. 使用如下命令进行压缩扩容镜像的制作

   以Ubuntu LTS 22.04，磁盘编号为`/dev/sdb`，保存镜像名称为`test.img`为例，注意发行版本入参末尾可以没有`/`，脚本中会进行判断。

   - 基础命令

     ```shell
     bash base.sh ubuntu/22.04 /dev/sdb test.img
     ```

   - 如果您希望用路径下的`config.ini`刷新网络配置，请添加参数`n`

     ```shell
     bash base.sh -n ubuntu/22.04 /dev/sdb test.img
     ```
     
   - 如果您希望制作好镜像后自动进行压缩，请添加参数`c`，不过如果机器性能不好不建议
   
     ```shell
     bash base.sh -c ubuntu/22.04 /dev/sdb test.img
     ```
   
   同样上述选项可以组合使用。

## 6 问题

1. miio下载

   miio下载太慢了，而且可能有的pip源还没有，所以采用直接从源码下载的方式。但是由于未知原因，Github的访问又很慢，所以该步骤有可能会失败。不过幸运的是这个依赖只有官方提供的语音样例会使用到，所以如果下载失败了也无伤大雅。

2. 本地桌面firefox下载

   为了避免使用snap下载firefox，配置了firefox的ppa源，不过同样由于未知原因，ppa源的添加可能会失败，如果失败，建议重试几次，或者挑一个网速可以的时间碰碰运气。

3. 本地桌面安装时配置connman导致ssh断连

   connman是用来配置网络的工具，在`apt`对它进行`setting up`的时候会导致`ssh`断连，同时它会弹出一个图形化界面来让用户进行配置，不过由于我们制作镜像的时候是使用的命令行工具，因此只会看到`ssh`断连的现象。如果发生了断连现象，需要重新登录后，将`apt`的进程`kill`后，再次执行脚本。

4. 中文输入法

   目前没有找到如何通过命令直接将默认输入法改为中文，目前还是只能安装好输入法后在图形化界面添加中文输入法并改变优先级。

## 7 贡献


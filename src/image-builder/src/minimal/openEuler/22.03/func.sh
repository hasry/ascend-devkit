#!/usr/bin/env bash

get_base_system() {
  set -e
  # 下载所需发行版的镜像文件
  log "Download $distribution_name image."
  if ls "$path_download"/"$distribution_name"*"$version"*LTS*aarch64*.iso > /dev/null 2>&1; then
    distribution_image_name=$(ls "$path_download"/"$distribution_name"*"$version"*LTS*aarch64*.iso) > /dev/null 2>&1
    over_amount_flag=$(echo "$distribution_image_name" | awk -F' ' '{print $2}')
    if [ "$over_amount_flag" != "" ]; then
      log "Please provide UNIQUE $distribution_name image."
      return 1
    fi
  fi
  if [ "$distribution_image_name" == "" ]; then
    distribution_image_url="$(jq '.source.url.base_system' "$config_path")"
    if [ "$distribution_image_url" == "" ]; then
      log "Please provide $distribution_name image url OR $distribution_name LTS version image in \"$path_download\"."
      return 1
    fi
    distribution_image_name=$(echo "$distribution_image_url" | rev | cut -d/ -f1 | rev)
    distribution_image_backup_name=${distribution_image_name%.*}\.backup\.${distribution_image_name##*.}
    wget "$distribution_image_url" -O "$path_download"/"$distribution_image_backup_name" --no-check-certificate > /dev/null 2>&1
    mv "$path_download"/"$distribution_image_backup_name" "$path_download"/"$distribution_image_name"
  fi
}

get_file_system() {
  set -e
  # 直接下载官方提供的定制文件系统
  log "Download customized file system."
  if ls "$path_download"/*Sample*"$distribution_name"* > /dev/null 2>&1; then
    customized_file_system_name=$(ls "$path_download"/*Sample*"$distribution_name"*) > /dev/null 2>&1
    over_amount_flag=$(echo "$customized_file_system_name" | awk -F' ' '{print $2}')
    if [ "$over_amount_flag" != "" ]; then
      log "Please provide UNIQUE customized file system image."
    fi
  fi
  if [ "$customized_file_system_name" == "" ]; then
    if ls "$path_download"/Ascend*sample-root-filesystem*.zip > /dev/null 2>&1; then
      customized_file_system_zip_name=$(ls "$path_download"/Ascend*sample-root-filesystem*.zip) > /dev/null 2>&1
      over_amount_flag=$(echo "$customized_file_system_name" | awk -F' ' '{print $2}')
      if [ "$over_amount_flag" != "" ]; then
        log "Please provide UNIQUE customized file system zip package."
        return 1
      fi
    fi
    if [ "$customized_file_system_zip_name" == "" ]; then
      customized_file_system_zip_url="$(jq '.source.url.file_system' "$config_path")"
      if [ "$customized_file_system_zip_url" == "" ]; then
         log "Please provide customized file system zip url to download OR customized file system image in \"$path_download\"."
         return 1
      fi
      customized_file_system_zip_name=$(echo "$customized_file_system_zip_url" | rev | cut -d/ -f1 | rev)
      customized_file_system_zip_backup_name=${customized_file_system_zip_name%.*}\.backup\.${customized_file_system_zip_name##*.}
      wget "$customized_file_system_zip_url" -O "$path_download"/"$customized_file_system_zip_backup_name" --no-check-certificate > /dev/null 2>&1
      mv "$path_download"/"$customized_file_system_zip_backup_name" "$path_download"/"$customized_file_system_zip_name"
      customized_file_system_zip_name="$path_download"/"$customized_file_system_zip_name"
    fi
    customized_file_system_name="$(unzip -l "$customized_file_system_zip_name" | grep "Sample.*$distribution_name" | sed 's/.*[[:space:]]\{1,\}\([^[:space:]]\{1,\}\)$/\1/')"
    unzip -j "$customized_file_system_zip_name" "$customized_file_system_name" -d "$path_download" > /dev/null 2>&1
  fi
}

get_npu_driver() {
  set -e
  # 下载驱动文件
  log "Download npu driver."
  if ls "$path_download"/Ascend*npu*driver*.run > /dev/null 2>&1; then
    npu_driver_name=$(ls "$path_download"/Ascend*npu*driver*.run) > /dev/null 2>&1
    over_amount_flag=$(echo "$npu_driver_name" | awk -F' ' '{print $2}')
    if [ "$over_amount_flag" != "" ]; then
      log "Please provide UNIQUE driver run package."
      return 1
    fi
  fi
  if [ "$npu_driver_name" == "" ]; then
    npu_driver_url="$(jq '.source.url.npu' "$config_path")"
    if [ "$npu_driver_url" == "" ]; then
      log "Please provide driver run package url OR direct driver run package in \"$path_download\"."
      return 1
    fi
    npu_driver_name=$(echo "$npu_driver_url" | rev | cut -d/ -f1 | rev)
    npu_driver_backup_name=${npu_driver_name%.*}\.backup\.${npu_driver_name##*.}
    wget "$npu_driver_url" -O "$path_download"/"$npu_driver_backup_name" --no-check-certificate > /dev/null 2>&1
    mv "$path_download"/"$npu_driver_backup_name" "$path_download"/"$npu_driver_name"
  fi
}

get_hdk() {
  set -e
  # 下载镜像工具文件
  log "Download ascend hdk."
  path_sdtool="$path_download"/sdtool
  if ! ls "$path_sdtool" > /dev/null 2>&1; then
    path_sdtool_tar="$path_sdtool".tar.gz
    if ! ls "$path_sdtool_tar" > /dev/null 2>&1; then
      if ls "$path_download"/Ascend*hdk*sdk*soc*.zip > /dev/null 2>&1; then
        ascend_hdk_name=$(ls "$path_download"/Ascend*hdk*sdk*soc*.zip) > /dev/null 2>&1
        over_amount_flag=$(echo "$ascend_hdk_name" | awk -F' ' '{print $2}')
        if [ "$over_amount_flag" != "" ]; then
          log "Please provide UNIQUE ascend hdk zip pacakge."
          return 1
        fi
      fi
      if [ "$ascend_hdk_name" == "" ]; then
        ascend_hdk_url="$(jq '.source.url.hdk' "$config_path")"
        if [ "$ascend_hdk_url" == "" ]; then
          log "Please provide ascend hdk url to download OR direct ascend hdk zip package."
          return 1
        fi
        ascend_hdk_name=$(echo "$ascend_hdk_url" | rev | cut -d/ -f1 | rev)
        ascend_hdk_backup_name=${ascend_hdk_name%.*}\.backup\.${ascend_hdk_name##*.}
        wget "$ascend_hdk_url" -O "$path_download"/"$ascend_hdk_backup_name" --no-check-certificate > /dev/null 2>&1
        mv "$path_download"/"$ascend_hdk_backup_name" "$path_download"/"$ascend_hdk_name"
        ascend_hdk_name="$path_download"/"$ascend_hdk_name"
      fi
      unzip -j "$ascend_hdk_name" Ascend310B-sdk/"$(echo "$path_sdtool_tar" | rev | cut -d '/' -f 1 | rev)" -d "$path_download" > /dev/null 2>&1
    fi
    tar -xzvf "$path_sdtool_tar" -C "$path_download"/ > /dev/null 2>&1
  fi
  cp -arf  "${path_sdtool:?}"/* "$path_download"/
}

# 设备写入函数
write_to_device() {
  set -e
  # 开始向目标设备写入内容
  log "Begin to write to device $dev_name."
  set +e
  "$path_download"/emmc-head --help > /dev/null 2>&1
  set -e
  # 修改镜像工具配置文件
  echo "#\041/bin/bash
MAKE_IMGPK_FLAG=off
FS_BACKUP_FLAG=off
ROOT_PART_SIZE=20480
LOG_PART_SIZE=1024
HOME_DATA_SIZE=2048" > "$path_download"/mksd.conf
  cd "$path_download"
  expect <<-END > /dev/null 2>&1
      # 设置超时时间为10分钟
      set timeout 360
      # 开始制卡
      spawn python3 $path_download/make_sd_card.py local $dev_name
      expect "Begin to make SD Card*"
      # 确认已经下载好所有依赖
      send "Y\n"
      # 等待完成的返回内容
      expect "Make SD Card successfully!"
      exit
END
  cd - > /dev/null 2>&1
  log "Finish writing to $dev_name."
}

post_process() {
  set -e
  log "Conduct post processing."
  mount "$dev_name"2 "$path_mount"
  sync
  sed -i "s/http:\/\/repo.openeuler.org\/openEuler-22.03-LTS\//https:\/\/repo.huaweicloud.com\/openeuler\/openEuler-22.03-LTS\//" "$path_mount"/etc/yum.repos.d/openEuler.repo
  printf "TYPE=Ethernet
BOOTPROTO=dhcp
DEFROUTE=no
NAME=eth0
DEVICE=eth0
ONBOOT=yes" > "$path_mount"/etc/sysconfig/network-scripts/ifcfg-eth0
  printf "TYPE=Ethernet
BOOTPROTO=static
DEFROUTE=yes
NAME=eth1
DEVICE=eth1
ONBOOT=yes
IPADDR=192.168.137.100
NETMASK=255.255.255.0
GATEWAY=192.168.137.1
DNS1=8.8.8.8
DNS2=114.114.114.114" > "$path_mount"/etc/sysconfig/network-scripts/ifcfg-eth1
  printf "TYPE=Ethernet
BOOTPROTO=static
DEFROUTE=yes
NAME=usb0
DEVICE=usb0
ONBOOT=yes
IPADDR=192.168.0.2
NETMASK=255.255.255.0
GATEWAY=192.168.0.1
DNS1=8.8.8.8
DNS2=114.114.114.114" > "$path_mount"/etc/sysconfig/network-scripts/ifcfg-usb0
  sleep 3
  umount "$path_mount"
  cleanup_temporary_files "$path_mount"
  cleanup_temporary_files "$PARENT_PATH"/minirc_install_hook.sh
  cleanup_temporary_files "$PARENT_PATH"/boot_image_info
  cleanup_temporary_files "$PARENT_PATH"/parttion_head_info
}

# 定制化处理函数
accident_handler() {
  cleanup_temporary_files "$path_download/$distribution_image_backup_name"
  cleanup_temporary_files "$path_download/$customized_file_system_zip_backup_name"
  cleanup_temporary_files "$path_download/$npu_driver_backup_name"
  cleanup_temporary_files "$path_download/$ascend_hdk_backup_name"
  cleanup_temporary_files "$path_download"
  cleanup_temporary_files "$path_mount"
  cleanup_temporary_files "$PARENT_PATH"/minirc_install_hook.sh
  cleanup_temporary_files "$PARENT_PATH"/boot_image_info
  cleanup_temporary_files "$PARENT_PATH"/parttion_head_info
}

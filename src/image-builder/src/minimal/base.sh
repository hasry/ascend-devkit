#!/usr/bin/env bash

set -e

USAGE="Usage: bash $(basename "$0") \"distribution path\" \"target disk\"
[\"dependencies' download path\"] [\"mount path for post process\"].
Script will find the distribution path below parent path, which provides
cfg.json and func.sh to finish the whole complete image build process."

while getopts "hrv" opt; do
  case "$opt" in
  h)
    printf "%s\\n" "$USAGE"
    exit 2
    ;;
  ?)
    echo "Usage: bash $(basename "$0") [-v] \"distribution path\" \"target disk\" [\"dependencies' download path\"]
[\"mount path for post process\"], more detailed information please run bash $(basename "$0") -h."
    exit 1
    ;;
  esac
done
shift "$((OPTIND - 1))"

# 环境变量
# 脚本名称
SCRIPT_NAME=${BASH_SOURCE[0]}
# 获取当前文件路径
PARENT_PATH=$(cd "$(dirname "$SCRIPT_NAME")" && pwd)
# 配置文件名称
CONFIG_FILE_NAME="cfg.json"
# 函数仓库名称
FUNCTION_FILE_NAME="func.sh"
# 发行版名称代号
distribution_name="$(realpath "$PARENT_PATH"/"$1" | rev | cut -d '/' -f2 | rev)"
# 版本号
version="$(realpath "$PARENT_PATH"/"$1" | rev | cut -d '/' -f1 | rev)"
# 用户目标磁盘
dev_name="$2"
# 用户指定的依赖下载路径
path_download="$3"
# 用户指定后处理挂载路径
path_mount="$4"

# 日志函数
log() {
  set -e
  cur_date=$(date "+%Y-%m-%d %H:%M:%S")
  echo -e "[$cur_date] [CREATE MINIMAL IMAGE] [$distribution_name $version] ""$1"
}

# 异常结束处理函数
termination_handler() {
  log "The script will stop for some reasons."
  if [ "$(type -t accident_handler)" = "function" ]; then
    accident_handler
  fi
  # 回到用户调用脚本的当前路径
  cd "$(pwd)"
  # 关闭报错即退出
  set +e
}

# 中断处理函数
interrupt_handler() {
  # 取消ERR标志的trap以防后面的ERR进入循环
  trap - ERR
  printf "\n"
  termination_handler
  log "The script has been terminated by ctrl c."
  exit
}

# 权限检查函数
authority_check_base() {
  set -e
  if [ "$EUID" -ne 0 ]; then
    log "Please use root to run this script."
    exit 1
  fi
  export USER=root
  if [ "$(umask)" != "0022" ]; then
    log "Please check umask value of root because it should be 0022."
    exit 1
  fi
}

# 入参检查函数
parameter_check_base() {
  set -e
  if [ "$distribution_name" == "" ]; then
    log "Distribution name must be provided."
    exit 1
  fi
  if [ "$version" == "" ]; then
    log "Version must be provided."
    exit 1
  fi
  if [ "$dev_name" == "" ]; then
    log "Device name must be provided."
    exit 1
  fi

  if [ "$path_download" == "" ]; then
    path_download="$PARENT_PATH/$distribution_name/$version"/download
  fi
  path_download=${path_download%/}
  mkdir -p "$path_download"
  if [ "$path_mount" == "" ]; then
    path_mount="$PARENT_PATH/$distribution_name/$version"/mnt
  fi
  path_mount=${path_mount%/}
  mkdir -p "$path_mount"
}

# 依赖下载函数
install_dependencies_base() {
  set -e
  log "Install dependencies for base."
  # 处理配置文件的依赖
  apt_required=("jq")
  # sdtool依赖
  apt_required+=("qemu-user-static" "binfmt-support" "dosfstools" "dump" "parted" "kpartx" "gcc" "g++")
  # 已下载依赖数组
  apt_installed=()
  # 未下载依赖数组
  apt_to_install=()
  for package in "${apt_required[@]}"; do
    if dpkg -l "$package" > /dev/null 2>&1; then
      apt_installed+=("$package")
    else
      apt_to_install+=("$package")
    fi
  done
  mapfile -t apt_installed < <(for package in "${apt_installed[@]}"; do echo "${package}"; done | sort -u)
  mapfile -t apt_to_install < <(for package in "${apt_to_install[@]}"; do echo "${package}"; done | sort -u)
  apt_installed_str="These packages have been installed:"
  for package in "${apt_installed[@]}"; do
    apt_installed_str+=" $package"
  done
  if [ ${#apt_installed[@]} -ne 0 ]; then log "$apt_installed_str."; fi
  if [ ${#apt_to_install[@]} -ne 0 ]; then
    apt_to_install_log_str="These packages are to install:"
    apt_to_install_str="apt-get -f --no-install-recommends install"
    for package in "${apt_to_install[@]}"; do
      apt_to_install_log_str+=" $package"
      apt_to_install_str+=" $package"
    done
    apt_to_install_str+=" -y"
    log "$apt_to_install_log_str."
    apt-get update > /dev/null 2>&1
    $apt_to_install_str > /dev/null 2>&1
  else
    log "No packages to install."
  fi
}

# 配置文件处理函数
# 输入参数: 系统名称 系统版本 依赖下载路径 依赖安装路径
# 下载路径指的是wifi的ko, miniconda, CANN, mxVision等安装脚本目录, 默认为当前脚本路径下的download
# 安装路径指的是Miniconda3, CANN, mxVision等依赖在安装的时候的目录, 默认为/usr/local
process_config_base() {
  set -e
  # 构造变量
  # 函数使能标志
  enabled_flag="y"
  disabled_flag="n"
  # 配置文件和函数定义文件路径
  config_path="$PARENT_PATH/$distribution_name/$version/$CONFIG_FILE_NAME"
  func_path="$PARENT_PATH/$distribution_name/$version/$FUNCTION_FILE_NAME"

  # 获取配置yaml文件所有的key, 并构建函数数组
  log "Build required-functions array before running."
  IFS=" " read -r -a functions_array <<< "$(jq -r --arg enabled_flag "$enabled_flag" '.function | to_entries | map(select(.value == $enabled_flag)) | map(.key) | @sh' "$config_path" | sed "s/'//g")"
}

# 运行函数
# 根据构建的函数数组运行相关函数
run_base() {
  set -e
  # 引入函数定义文件中的函数
  # shellcheck disable=SC1090
  source "$func_path"
  # 运行成功的函数的计数
  func_executed_num=0
  # 最后显示信息
  final_information=""

  # for循环运行所有待运行的函数
  for key in "${functions_array[@]}"; do
    # 运行相应函数
    if ! $"$key"; then
      return 1
    fi
    # 修改、配置文件
    cat <<< "$(jq --arg key "$key" --arg disabled_flag "$disabled_flag" '.function[$key]=$disabled_flag' "$config_path")" > "$config_path"
    # 运行成功后计数加一
    func_executed_num=$((func_executed_num + 1))
  done
  # 补充最后的显示信息
  final_information+=$"All the temp files are in \"${path_download}\", and user can remove them according to needs."
  # 计数满足后全部刷新为y以方便下次运行
  if [ "$func_executed_num" == "${#functions_array[@]}" ]; then
    for key in "${functions_array[@]}"; do
      cat <<< "$(jq --arg key "$key" --arg enabled_flag "$enabled_flag" '.function[$key]=$enabled_flag' "$config_path")" > "$config_path"
    done
  fi
  # 显示最后信息
  log "$final_information"
  return 0
}

# 临时文件清理函数
# 输入参数: 待删除的路径或者文件 空目录标识
# 先判断是否在path_download下面, 如果不是则直接按入参进行删除
# 当空目录标识为true时, 函数将不会判断目录是否为空, 直接删除
cleanup_temporary_files() {
  set -e
  if [ "$1" == "" ]; then
    return 0
  fi
  if [ ! -e "$1" ]; then
    return 0
  fi
  if [ -f "$1" ] || [ -z "$(ls -A "$1")" ] || [ "$2" == true ]; then
    rm -r "$1"
    return 0
  fi
}

# 主函数
main() {
  set -e
  # 将异常结束函数赋予ERR标志
  trap termination_handler ERR
  # 将中断处理函数赋予SIGINT标志
  trap interrupt_handler SIGINT
  if ! authority_check_base || ! parameter_check_base || ! install_dependencies_base || ! process_config_base || ! run_base; then
    return 1
  fi
  # 回到用户调用脚本的当前路径
  cd "$(pwd)"
  # 关闭报错即退出
  set +e
}

if ! main; then
  log "Minimal image build failed!"
else
  log "Minimal image build successful!"
fi

#!/usr/bin/env bash

# 预处理函数
pre_process() {
  set -e
  # 修改时区
  log "Modify local timezone."
  if [ "$FLAG_VERBOSE" == true ]; then
    timedatectl set-timezone Asia/Shanghai
  else
    timedatectl set-timezone Asia/Shanghai > /dev/null 2>&1
  fi
  # 删除固定usb0mac地址的系统文件
  if [ -f /usr/lib/systemd/network/99-default.link ]; then
    rm /usr/lib/systemd/network/99-default.link
  fi
}

# apt-get 配置函数
apt_install() {
  set -e
  log "Begin to install apt dependencies."
  # 构建apt-get将要下载的数组
  apt_required=()
  # 镜像压缩和扩容所需依赖
  apt_required+=("dosfstools" "dos2unix" "expect" "parted" "dump")
  # 系统所需依赖
  apt_required+=("git" "gcc" "g++" "software-properties-common")
  # 视频解析以及外设等依赖
  apt_required+=("ffmpeg" "v4l-utils")
  # CANN所需依赖
  apt_required+=("g++" "make" "cmake" "zlib1g" "zlib1g-dev" "openssl" "libsqlite3-dev" "libssl-dev" "libffi-dev" "unzip" "pciutils" "net-tools" "libblas-dev" "gfortran" "libblas3")
  # 登录文字修改
  apt_required+=("figlet" "lolcat")
  # 图形化界面、Wifi所需依赖
  apt_required+=("network-manager")
  # 解决git Gnu TLS报错
  apt_required+=("gnutls-bin")
  # 已下载依赖数组
  apt_installed=()
  # 未下载依赖数组
  apt_to_install=()
  for package in "${apt_required[@]}"; do
    if dpkg -l "$package" > /dev/null 2>&1; then
      apt_installed+=("$package")
    else
      apt_to_install+=("$package")
    fi
  done
  mapfile -t apt_installed < <(for package in "${apt_installed[@]}"; do echo "${package}"; done | sort -u)
  mapfile -t apt_to_install < <(for package in "${apt_to_install[@]}"; do echo "${package}"; done | sort -u)
  apt_installed_str="These packages have been installed:"
  for package in "${apt_installed[@]}"; do
    apt_installed_str+=" $package"
  done
  if [ ${#apt_installed[@]} -ne 0 ]; then log "$apt_installed_str."; fi
  if [ ${#apt_to_install[@]} -ne 0 ]; then
    apt_to_install_log_str="These packages are to install:"
    apt_to_install_str="apt-get -f --no-install-recommends install"
    for package in "${apt_to_install[@]}"; do
      apt_to_install_log_str+=" $package"
      apt_to_install_str+=" $package"
    done
    apt_to_install_str+=" -y"
    log "$apt_to_install_log_str."
    if [ "$FLAG_VERBOSE" == true ]; then
        apt-get update
        $apt_to_install_str
    else
        apt-get update > /dev/null 2>&1
        $apt_to_install_str > /dev/null 2>&1
    fi
  else
    log "No packages to install."
  fi
  log "Finish installing apt dependencies."
}

# miniconda配置函数
install_miniconda() {
  set -e
  log "Begin to install miniconda."
  if ! activate_conda > /dev/null 2>&1; then
    # 下载miniconda的安装文件
    log "Download miniconda."
    miniconda_url="$(jq -r '.source.url.miniconda' "$config_path")"
    if [ "$miniconda_url" != "" ]; then
      miniconda_file_name=$(echo "$miniconda_url" | rev | cut -d/ -f1 | rev)
    else
      miniconda_file_name=$(find "$path_download"/*conda* | awk -F'[/]' '{print $NF}')
    fi
    over_amount_flag=$(echo "$miniconda_file_name" | awk -F' ' '{print $2}')
    if [ "$over_amount_flag" != "" ]; then
      log "Please provide unique miniconda install scripts."
      return 1
    fi
    miniconda_file_backup_name=${miniconda_file_name%.*}\.backup\.${miniconda_file_name##*.}
    # 设置miniconda安装目录名
    miniconda3_installed_directory_name="miniconda3"
    if [ "$miniconda_url" == "" ] && [ ! -e "$path_install"/"$miniconda3_installed_directory_name" ] && [ ! -f "$path_download"/"$miniconda_file_name" ]; then
      log "Provide miniconda download url or file."
      return 1
    fi
    if [ ! -e "$path_install"/"$miniconda3_installed_directory_name" ]; then
      if [ ! -f "$path_download"/"$miniconda_file_name" ]; then
        if [ "$FLAG_VERBOSE" == true ]; then
          wget "$miniconda_url" -O "$path_download"/"$miniconda_file_backup_name" --no-check-certificate
        else
          wget "$miniconda_url" -O "$path_download"/"$miniconda_file_backup_name" --no-check-certificate > /dev/null 2>&1
        fi
        mv "$path_download"/"$miniconda_file_backup_name" "$path_download"/"$miniconda_file_name"
      fi
    fi
    # 安装miniconda
    log "Install miniconda."
    if [ "$FLAG_VERBOSE" == true ]; then
      bash "$path_download"/"$miniconda_file_name" -b -f -p "$path_install"/"$miniconda3_installed_directory_name"
    else
      bash "$path_download"/"$miniconda_file_name" -b -f -p "$path_install"/"$miniconda3_installed_directory_name" > /dev/null 2>&1
    fi
    # 配置miniconda在root和HwHiAiUser用户登录时都为自启动
    log "Miniconda automatic initialization."
    if [[ $(cat < /root/.bashrc | grep "# >>> conda initialize >>>") == "" ]]; then
      echo "# >>> conda initialize >>>
# \041\041 Contents within this block are managed by 'conda init' \041\041
__conda_setup=\"\$('$path_install/miniconda3/bin/conda' 'shell.bash' 'hook' 2> /dev/null)\"
if [ \$? -eq 0 ]; then
   eval \"\$__conda_setup\"
else
   if [ -f \"$path_install/miniconda3/etc/profile.d/conda.sh\" ]; then
       . \"$path_install/miniconda3/etc/profile.d/conda.sh\"
   else
       export PATH=\"$path_install/miniconda3/bin:\$PATH\"
   fi
fi
unset __conda_setup
# <<< conda initialize <<<" >> /root/.bashrc
    fi
    if [[ $(cat < /home/HwHiAiUser/.bashrc | grep "# >>> conda initialize >>>") == "" ]]; then
      echo "# >>> conda initialize >>>
# \041\041 Contents within this block are managed by 'conda init' \041\41
__conda_setup=\"\$('$path_install/miniconda3/bin/conda' 'shell.bash' 'hook' 2> /dev/null)\"
if [ \$? -eq 0 ]; then
   eval \"\$__conda_setup\"
else
   if [ -f \"$path_install/miniconda3/etc/profile.d/conda.sh\" ]; then
       . \"$path_install/miniconda3/etc/profile.d/conda.sh\"
   else
       export PATH=\"$path_install/miniconda3/bin:\$PATH\"
   fi
fi
unset __conda_setup
# <<< conda initialize <<<" >> /home/HwHiAiUser/.bashrc
    fi

    # 修改miniconda的下载源
    log "Modify downloading sources of miniconda."
    conda_source_cfg=$(jq -r '.conda.source' "$config_path")
    printf "%s" "$conda_source_cfg" > "$path_install"/.condarc
fi

  # conda激活
  if ! activate_conda; then
    return 1
  fi

  if [ "$FLAG_VERBOSE" == true ]; then
    # 确保miniconda下载源已更新
    log "Clean conda downloading sources to use new sources."
    conda clean -i -y
    # 下载python3.9.2
    log "Downloading python 3.9.2."
    if [ "$(python --version | awk -F ' ' '{print $2}')" != "3.9.2" ]; then
      conda config --append channels conda-forge
      # insecure 模式下载python3.9.2以绕过ssl认证
      conda install python=3.9.2 -k -y
    fi
  else
    # 确保miniconda下载源已更新
    log "Clean conda downloading sources to use new sources."
    conda clean -i -y > /dev/null 2>&1
    # 下载python3.9.2
    log "Downloading python 3.9.2."
    if [ "$(python --version | awk -F ' ' '{print $2}')" != "3.9.2" ]; then
      conda config --append channels conda-forge > /dev/null 2>&1
      # insecure 模式下载python3.9.2以绕过ssl认证
      conda install python=3.9.2 -k -y > /dev/null 2>&1
    fi
  fi
  log "Finish installing miniconda."
}

activate_conda() {
  #conda初始化
  if __conda_setup="$("$path_install/miniconda3/bin/conda" 'shell.bash' 'hook' 2>/dev/null)"; then
    eval "$__conda_setup"
  else
    if [ -f "$path_install/miniconda3/etc/profile.d/conda.sh" ]; then
      . "$path_install/miniconda3/etc/profile.d/conda.sh"
    else
      export PATH="$path_install/miniconda3/bin:$PATH"
    fi
  fi
  unset __conda_setup
  # conda环境激活
  if ! conda activate; then
    return 1
  fi
}

# pip配置函数
python_pip_install() {
  set -e
  log "Begin to install python dependencies."
  # conda激活
  if ! activate_conda; then
    return 1
  fi

  # 若不存在，则新建~/.pip目录
  mkdir -p ~/.pip
  # 配置pip下载源为豆瓣源
  log "Modify pip downloading sources."
  pip_source_cfg=$(jq -r '.python.source' "$config_path")
  if [ "$pip_source_cfg" != "" ]; then
    printf "%s" "$pip_source_cfg" | sed "s/'\|\"//g" > ~/.pip/pip.conf
  fi

  # pip 第三方依赖
  pip_dependencies_array=("torch==1.13.0" "torchaudio==0.13.0" "torchvision==0.14.0" "scikit-video" "ipywidgets" "ffmpeg-python" "IPython==7.34.0" "numpy==1.22.4" "albumentations" "jupyterlab" "matplotlib" "pycocotools" "protobuf==3.20.0")
  # CANN pip依赖
  pip_dependencies_array+=("attrs" "numpy" "decorator" "cffi" "pyyaml" "pathlib2" "psutil" "protobuf" "scipy" "requests" "absl-py")
  # atc python依赖
  pip_dependencies_array+=("sympy")
  mapfile -t pip_install_array < <(for package in "${pip_dependencies_array[@]}"; do echo "${package}"; done | sort -u)
  pip_str="pip install --no-input"
  for package in "${pip_install_array[@]}"; do
    pip_str+=" $package"
  done
  # acl-runtime 安装包名称
  acl_runtime_url="$(jq -r '.source.url["acl-runtime"]' "$config_path")"
  acl_runtime_file_name=$(echo "$acl_runtime_url" | rev | cut -d/ -f1 | rev)
  acl_runtime_file_backup_name=${acl_runtime_file_name%.*}\.backup\.${acl_runtime_file_name##*.}
  # ais_bench 安装包名称
  ais_bench_url="$(jq -r '.source.url.ais_bench' "$config_path")"
  ais_bench_file_name=$(echo "$ais_bench_url" | rev | cut -d/ -f1 | rev)
  ais_bench_file_backup_name=${ais_bench_file_name%.*}\.backup\.${ais_bench_file_name##*.}
  # shellcheck disable=SC2001
  pip_log_str=$(echo "$pip_str" | sed 's/^\(.\)/\U\1/')
  pip_log_str+=" python-miio acl-runtime ais_bench"

  if [ "$FLAG_VERBOSE" == true ]; then
    log "$pip_log_str."
    $pip_str
    # 使用豆瓣源下载python-miio，绕过ssl认证
    pip install git+https://github.com/rytilahti/python-miio.git
    # 下载aclruntime和ais_bench的wheel安装包
    if [ ! -f "$path_download"/"$acl_runtime_file_name" ]; then
      wget "$acl_runtime_url" -O "$path_download"/"$acl_runtime_file_backup_name" --no-check-certificate
      mv "$path_download"/"$acl_runtime_file_backup_name" "$path_download"/"$acl_runtime_file_name"
    fi
    pip install "$path_download"/"$acl_runtime_file_name" --no-input
    if [ ! -f "$path_download"/"$ais_bench_file_name" ]; then
      wget "$ais_bench_url" -O "$path_download"/"$ais_bench_file_backup_name" --no-check-certificate
      mv "$path_download"/"$ais_bench_file_backup_name" "$path_download"/"$ais_bench_file_name"
    fi
    pip install "$path_download"/"$ais_bench_file_name" --no-input
  else
    log "$pip_log_str."
    $pip_str > /dev/null 2>&1
    # 使用豆瓣源下载python-miio，绕过ssl认证
    pip install git+https://github.com/rytilahti/python-miio.git > /dev/null 2>&1
    # 下载aclruntime和ais_bench的wheel安装包
    if [ ! -f "$path_download"/"$acl_runtime_file_name" ]; then
      wget "$acl_runtime_url" -O "$path_download"/"$acl_runtime_file_backup_name" --no-check-certificate > /dev/null 2>&1
      mv "$path_download"/"$acl_runtime_file_backup_name" "$path_download"/"$acl_runtime_file_name"
    fi
    pip install "$path_download"/"$acl_runtime_file_name" --no-input > /dev/null 2>&1
    if [ ! -f "$path_download"/"$ais_bench_file_name" ]; then
      wget "$ais_bench_url" -O "$path_download"/"$ais_bench_file_backup_name" --no-check-certificate > /dev/null 2>&1
      mv "$path_download"/"$ais_bench_file_backup_name" "$path_download"/"$ais_bench_file_name"
    fi
    pip install "$path_download"/"$ais_bench_file_name" --no-input > /dev/null 2>&1
  fi
  log "Finish installing python dependencies."
}

# CANN下载安装函数
install_cann() {
  set -e
  log "Begin to add CANN."
  # conda激活
  if ! activate_conda; then
    return 1
  fi

  # 下载并安装CANN
  log "Download CANN."
  if find "$path_download"/*cann* > /dev/null 2>&1; then
    cann_file_name=$(find "$path_download"/*cann* | awk -F'[/]' '{print $NF}')
    over_amount_flag=$(echo "$cann_file_name" | awk -F' ' '{print $2}')
    if [ "$over_amount_flag" != "" ]; then
      log "Please provide unique cann install scripts."
      return 1
    fi
  fi
  if [ "$cann_file_name" == "" ]; then
    url_cann="$(jq -r '.source.url.cann' "$config_path")"
    if [ "$url_cann" == "" ]; then
      log "Please provide cann downloading url or cann install scripts."
      return 1
    fi
    cann_file_name=$(echo "$url_cann" | rev | cut -d/ -f1 | rev)
    cann_file_backup_name=${cann_file_name%.*}\.backup\.${cann_file_name##*.}
    if [ "$FLAG_VERBOSE" == true ]; then
      wget "$url_cann" -O "$path_download"/"$cann_file_backup_name" --no-check-certificate
      mv "$path_download"/"$cann_file_backup_name" "$path_download"/"$cann_file_name"
    else
      wget "$url_cann" -O "$path_download"/"$cann_file_backup_name" --no-check-certificate > /dev/null 2>&1
      mv "$path_download"/"$cann_file_backup_name" "$path_download"/"$cann_file_name" > /dev/null 2>&1
    fi
  fi
  log "Install CANN."
  chmod +x "$path_download"/"$cann_file_name"
  if [ "$FLAG_VERBOSE" == true ]; then
    "$path_download"/"$cann_file_name" --full
  else
    "$path_download"/"$cann_file_name" --full > /dev/null 2>&1
  fi
  if [ ! -e /root/.bashrc ]; then
    touch /root/.bashrc
  fi
  if [ "$(cat < /root/.bashrc | grep ascend-toolkit)" == "" ]; then
    printf "source %s/Ascend/ascend-toolkit/set_env.sh\n" "$path_install" >> /root/.bashrc
  fi
  if [ "$(cat < /home/HwHiAiUser/.bashrc | grep ascend-toolkit)" == "" ]; then
    printf "source %s/Ascend/ascend-toolkit/set_env.sh\n" "$path_install" >> /home/HwHiAiUser/.bashrc
  fi
  log "Finish adding CANN."
}

# mxVision下载安装函数
install_mxvision() {
  set -e
  log "Begin to add mxVision."
  # conda激活
  if ! activate_conda; then
    return 1
  fi

  # 下载并安装mxvision
  log "Download mxVision."
  if [ -f "$path_install"/Ascend/ascend-toolkit/set_env.sh ]; then
    source "$path_install"/Ascend/ascend-toolkit/set_env.sh
  fi
  if find "$path_download"/*mxvision* > /dev/null 2>&1; then
    mxvision_file_name=$(find "$path_download"/*mxvision* | awk -F'[/]' '{print $NF}')
    over_amount_flag=$(echo "$mxvision_file_name" | awk -F' ' '{print $2}')
    if [ "$over_amount_flag" != "" ]; then
      log "Please provide unique mxVision install scripts."
      return 1
    fi
  fi
  if [ "$mxvision_file_name" == "" ]; then
    url_mxvision="$(jq -r '.source.url.mxvision' "$config_path")"
    if [ "$url_mxvision" == "" ]; then
      log "Please provide mxVision downloading url or mxvision install scripts."
      return 1
    fi
    mxvision_file_name=$(echo "$url_mxvision" | rev | cut -d/ -f1 | rev)
    mxvision_file_backup_name=${mxvision_file_name%.*}\.backup\.${mxvision_file_name##*.}
    if [ "$FLAG_VERBOSE" == true ]; then
      wget "$url_mxvision" -O "$path_download"/"$mxvision_file_backup_name" --no-check-certificate
    else
      wget "$url_mxvision" -O "$path_download"/"$mxvision_file_backup_name" --no-check-certificate > /dev/null 2>&1
    fi
    mv "$path_download"/"$mxvision_file_backup_name" "$path_download"/"$mxvision_file_name"
  fi
  chmod 777 "$path_download"/"$mxvision_file_name"
  log "Install mxVision."
  if [ "$FLAG_VERBOSE" == true ]; then
    "$path_download"/"$mxvision_file_name" --install --install-path="$HOME"/Ascend
  else
    "$path_download"/"$mxvision_file_name" --install --install-path="$HOME"/Ascend > /dev/null 2>&1
  fi
  log "Finish adding mxVision."
}

# acllite下载安装函数
install_acllite() {
  set -e
  log "Begin to add acllite."

  # 下载并安装acllite
  log "Download acllite."
  path_acllite=$path_install/Ascend/thirdpart/aarch64
  mkdir -p "$path_acllite"
  if [ ! -e "$path_acllite"/acllite ]; then
    if [ ! -e "$path_download"/samples ]; then
      if [ "$FLAG_VERBOSE" == true ]; then
        git clone -b v0.8.0 https://gitee.com/ascend/samples.git "$path_download"/samples
      else
        git clone -b v0.8.0 https://gitee.com/ascend/samples.git "$path_download"/samples > /dev/null 2>&1
      fi
    fi
    log "Copy acllite to ascend path."
    cp -r "$path_download"/samples/python/common/acllite "$path_acllite"/acllite
  fi
  log "Finish adding acllite."
}

# atc配置函数
modify_atc() {
  set -e
  log "Beging to change dynamic link for atc."
  # 修改libstdc++的软链接路径
  so_target_link="$(readlink -f "$path_install"/miniconda3/lib/libstdc++.so)"
  so_6_target_link="$(readlink -f "$path_install"/miniconda3/lib/libstdc++.so.6)"
  if [ "$so_target_link" == "$path_install"/miniconda3/lib/libstdc++.so.6.0.29 ]; then
    mv "$path_install"/miniconda3/lib/libstdc++.so "$path_install"/miniconda3/lib/libstdc++.so.old
    ln -s /usr/lib/aarch64-linux-gnu/libstdc++.so.6 "$path_install"/miniconda3/lib/libstdc++.so
  fi
  if [ "$so_6_target_link" == "$path_install"/miniconda3/lib/libstdc++.so.6.0.29 ]; then
    mv "$path_install"/miniconda3/lib/libstdc++.so.6 "$path_install"/miniconda3/lib/libstdc++.so.6.old
    ln -s /usr/lib/aarch64-linux-gnu/libstdc++.so.6 "$path_install"/miniconda3/lib/libstdc++.so.6
  fi
  log "Finish changing dynamic link for atc."
}

# 音频安装函数
add_audio() {
  set -e
  log "Begin to build audio function."
  # 下载要更换的内核部分
  log "Download kernel for replacing."
  path_audio="$path_download"/audio
  if [ ! -e "$path_audio" ] || [ -z "$(ls -A "$path_audio")" ] > /dev/null 2>&1; then
    if find "$path_download"/*audio*.zip; then
      audio_zip_name=$(find "$path_download"/*audio*.zip | awk -F'[/]' '{print $NF}')
      over_amount_flag=$(echo "$audio_zip_name" | awk -F' ' '{print $2}')
      if [ "$over_amount_flag" != "" ]; then
        log "Please provide unique audio zip package."
        return 1
      fi
    fi
    if [ "$audio_zip_name" == "" ]; then
      url_audio="$(jq -r '.source.url.audio' "$config_path")"
      if [ "$url_audio" == "" ]; then
        og "Please provide audio downloading url or audio zip package."
        return 1
      fi
      audio_zip_name=$(echo "$url_audio" | rev | cut -d/ -f1 | rev)
      audio_zip_backup_name=${audio_zip_name%.*}\.backup\.${audio_zip_name##*.}
      if [ "$FLAG_VERBOSE" == true ]; then
        wget "$url_audio" -O "$path_download"/"$audio_zip_backup_name" --no-check-certificate
      else
        wget "$url_audio" -O "$path_download"/"$audio_zip_backup_name" --no-check-certificate > /dev/null 2>&1
      fi
      mv "$path_download"/"$audio_zip_backup_name" "$path_download"/"$audio_zip_name"
    fi
    path_audio_backup=$path_audio\.backup
    if [ "$FLAG_VERBOSE" == true ]; then
      unzip "$path_download"/"$audio_zip_name" -d "$path_audio_backup"
    else
      unzip "$path_download"/"$audio_zip_name" -d "$path_audio_backup" > /dev/null 2>&1
    fi
    mv "$path_audio_backup" "$path_audio"
  fi

  # 替换内核
  log "Replace kernel."
  if [ "$FLAG_VERBOSE" == true ]; then
    dd if="$path_audio"/Image of=/dev/mmcblk1 count=61440 seek=32768 bs=512
  else
    dd if="$path_audio"/Image of=/dev/mmcblk1 count=61440 seek=32768 bs=512 > /dev/null 2>&1
  fi

  # 下载音频所需依赖
  apt_audio_required=("alsa-tools" "alsa-utils")
  apt_audio_installed=()
  apt_audio_to_install=()
  for package in "${apt_audio_required[@]}"; do
    if [ "$(dpkg -l | grep "$package")" != "" ]; then
      apt_audio_installed+=("$package")
    else
      apt_audio_to_install+=("$package")
    fi
  done
  apt_audio_installed_str="These packages have been installed:"
  for package in "${apt_audio_installed[@]}"; do
    apt_audio_installed_str+=" $package"
  done
  if [ ${#apt_audio_installed[@]} -ne 0 ]; then log "$apt_audio_installed_str."; fi
  if [ ${#apt_audio_to_install[@]} -ne 0 ]; then
    apt_audio_to_install_log_str="These packages are to install:"
    apt_audio_to_install_str="apt-get -f install"
    for package in "${apt_audio_to_install[@]}"; do
      apt_audio_to_install_log_str+=" $package"
      apt_audio_to_install_str+=" $package"
    done
    apt_audio_to_install_str+=" -y"
    log "$apt_audio_to_install_log_str."
    if [ "$FLAG_VERBOSE" == true ]; then
        apt-get update
        $apt_audio_to_install_str
    else
        apt-get update > /dev/null 2>&1
        $apt_audio_to_install_str > /dev/null 2>&1
    fi
  else
    log "No packages to install."
  fi
  log "Finish building audio function."
}

# 本地图形化界面
add_local_desktop() {
  log "Begin to build local desktop function."
  # 配置firefox下载渠道
  if [ ! -f /etc/apt/sources.list.d/mozillateam-ubuntu-ppa-jammy.list ]; then
    log "Modify firefox downloading source."
    if [ "$FLAG_VERBOSE" == true ]; then
      sysctl net.ipv6.conf.all.disable_ipv6=1
      add-apt-repository ppa:mozillateam/ppa -y
    else
      sysctl net.ipv6.conf.all.disable_ipv6=1 > /dev/null 2>&1
      add-apt-repository ppa:mozillateam/ppa -y > /dev/null 2>&1
    fi
    # prioritize the apt version of firefox over the snap version
    printf "Package: *
Pin: release o=LP-PPA-mozillateam
Pin-Priority: 1001

Package: firefox
Pin: version 1:1snap1-0ubuntu2
Pin-Priority: -1
" > /etc/apt/preferences.d/mozilla-firefox
    # 配置launchpad反向代理
    printf "deb https://launchpad.proxy.ustclug.org/mozillateam/ppa/ubuntu/ jammy main" > /etc/apt/sources.list.d/mozillateam-ubuntu-ppa-jammy.list
  fi
  # 安装所需依赖
  log "Download local desktop apt dependencies."
  apt_local_desktop_required=()
  apt_local_desktop_required+=("lxqt" "network-manager-gnome" "sddm" "firefox" "fcitx" "fcitx-libpinyin" "language-pack-zh-hans" "libdrm-dev")
  apt_local_desktop_installed=()
  apt_local_desktop_to_install=()
  for package in "${apt_local_desktop_required[@]}"; do
    if [ "$(dpkg -l | grep "$package")" != "" ]; then
      apt_local_desktop_installed+=("$package")
    else
      apt_local_desktop_to_install+=("$package")
    fi
  done
  apt_local_desktop_installed_str="These packages have been installed:"
  for package in "${apt_local_desktop_installed[@]}"; do
    apt_local_desktop_installed_str+=" $package"
  done
  if [ ${#apt_local_desktop_installed[@]} -ne 0 ]; then log "$apt_local_desktop_installed_str."; fi
  if [ ${#apt_local_desktop_to_install[@]} -ne 0 ]; then
    apt_local_desktop_to_install_log_str="These packages are to install:"
    for package in "${apt_local_desktop_to_install[@]}"; do
      apt_local_desktop_to_install_log_str+=" $package"
      if [ "$package" == "sddm" ]; then
        sddm_flag=true
      else
        apt_local_desktop_to_install_str+=" $package"
      fi
    done
    log "$apt_local_desktop_to_install_log_str."
    if [ "$FLAG_VERBOSE" == true ]; then
        if ! apt-get update; then return 1; fi
        if [ "$apt_local_desktop_to_install_str" != "" ]; then
          apt_local_desktop_to_install_str="apt-get -f -y install ${apt_local_desktop_to_install_str}"
          $apt_local_desktop_to_install_str
        fi
        if [ "$sddm_flag" == true ]; then
          expect <<- END
            spawn apt-get -f install sddm -y
            expect "Default display manager:*"
            send "2\n"
            expect "$ "
            exit
END
        fi
    else
        if ! apt-get update > /dev/null 2>&1; then return 1; fi
        if [ "$apt_local_desktop_to_install_str" != "" ]; then
          apt_local_desktop_to_install_str="apt-get -f -y install ${apt_local_desktop_to_install_str}"
          $apt_local_desktop_to_install_str > /dev/null 2>&1
        fi
        if [ "$sddm_flag" == true ]; then
          expect <<- END > /dev/null 2>&1
            spawn apt-get -f install sddm -y
            expect "Default display manager:*"
            send "2\n"
            expect "$ "
            exit
END
        fi
    fi
  else
    log "No packages to install."
  fi

  # 更新lib和ko
  log "Download graphic libs and kos."
  path_graphic="$path_download"/graphic
  if [ ! -e "$path_graphic" ] || [ -z "$(ls -A "$path_graphic")" ] > /dev/null 2>&1; then
    if find "$path_download"/*graphic*.zip; then
      graphic_zip_name=$(find "$path_download"/*graphic*.zip | awk -F'[/]' '{print $NF}')
      over_amount_flag=$(echo "$graphic_zip_name" | awk -F' ' '{print $2}')
      if [ "$over_amount_flag" != "" ]; then
        log "Please provide unique graphic zip package."
        return 1
      fi
    fi
    if [ "$graphic_zip_name" == "" ]; then
      url_graphic="$(jq -r '.source.url.graphic' "$config_path")"
      if [ "$url_graphic" == "" ]; then
        og "Please provide graphic downloading url or graphic zip package."
        return 1
      fi
      graphic_zip_name=$(echo "$url_graphic" | rev | cut -d/ -f1 | rev)
      graphic_zip_backup_name=${graphic_zip_name%.*}\.backup\.${graphic_zip_name##*.}
      if [ "$FLAG_VERBOSE" == true ]; then
        wget "$url_graphic" -O "$path_download"/"$graphic_zip_backup_name" --no-check-certificate
      else
        wget "$url_graphic" -O "$path_download"/"$graphic_zip_backup_name" --no-check-certificate > /dev/null 2>&1
      fi
      mv "$path_download"/"$graphic_zip_backup_name" "$path_download"/"$graphic_zip_name"
    fi
    path_graphic_backup=$path_graphic\.backup
    if [ "$FLAG_VERBOSE" == true ]; then
      unzip "$path_download"/"$graphic_zip_name" -d "$path_graphic_backup"
    else
      unzip "$path_download"/"$graphic_zip_name" -d "$path_graphic_backup" > /dev/null 2>&1
    fi
    mv "$path_graphic_backup" "$path_graphic"
  fi

  # 更换dtb
  log "Update dtb."
  if [ "$FLAG_VERBOSE" == true ]; then
    dd if="$path_graphic"/dt.img of=/dev/mmcblk1 count=4096 seek=114688 bs=512
    dd if="$path_graphic"/dt.img of=/dev/mmcblk1 count=4096 seek=376832 bs=512
  else
    dd if="$path_graphic"/dt.img of=/dev/mmcblk1 count=4096 seek=114688 bs=512 > /dev/null 2>&1
    dd if="$path_graphic"/dt.img of=/dev/mmcblk1 count=4096 seek=376832 bs=512 > /dev/null 2>&1
  fi

  # 复制ko到相应路径
  if [ ! -f /lib/modules/5.10.0+/ascend_vdp_drm.ko  ]; then
    cp "$path_graphic"/ascend_vdp_drm.ko /lib/modules/5.10.0+/
  fi
  if [ "$(lsmod | grep ascend_vdp_drm)" == "" ]; then
      insmod /lib/modules/5.10.0+/ascend_vdp_drm.ko
  fi

  # 配置本地桌面服务，设置图形桌面驱动自动加载
  log "Create and turn on local desktop service."
  printf "#\041/bin/bash
insmod /lib/modules/5.10.0+/ascend_vdp_drm.ko
nmcli radio wifi on
" > /usr/local/bin/toggle_graphical_desktop.sh
  sudo chmod +x /usr/local/bin/toggle_graphical_desktop.sh
  printf "[Unit]
Description=%s
Wants=start-davinci.service
After=start-davinci.service

[Service]
ExecStart=%s
Restart=on-failure
RestartSec=10s

[Install]
WantedBy=multi-user.target" /usr/local/bin/toggle_graphical_desktop.sh /usr/local/bin/toggle_graphical_desktop.sh > /etc/systemd/system/toggle_graphical_desktop.service
  systemctl daemon-reload
  if [ "$FLAG_VERBOSE" == true ]; then
    systemctl enable toggle_graphical_desktop
  else
    systemctl enable toggle_graphical_desktop > /dev/null 2>&1
  fi

  # 隐藏无关用户
  log "Hide unrelated users."
  # 配置文件路径
  mkdir -p /etc/sddm.conf.d
  # 添加隐藏用户配置到 sddm.conf 文件
  echo "[Users]
HideUsers=HwBaseUser,HwDmUser,HwSysUser" > /etc/sddm.conf.d/sddm.conf
  # 重启 sddm 服务使配置生效
  systemctl restart sddm
  # 修改lxqt默认壁纸
  cp "$path_graphic"/ascend-wallpaper.png /usr/share/lxqt/themes/debian/
  sed -i 's|^Wallpaper=.*$|Wallpaper=/usr/share/lxqt/themes/debian/ascend-wallpaper.png|' /etc/xdg/pcmanfm-qt/lxqt/settings.conf

  log "Set up fcitx."
  # 设置fcitx为默认输入法
  if [ "$(im-config -m | grep fcitx)" == "" ]; then
      im-config -n fcitx
  fi
  # 为root和HwHiAiUser用户添加中文键盘布局
  if [ "$(pgrep -f "fcitx")" == "" ]; then
    if [ "$FLAG_VERBOSE" == true ]; then
      expect <<- END
        spawn fcitx start
        expect "*not initialized"
        send "\n"
        exit
END
    else
      expect <<- END > /dev/null 2>&1
        spawn fcitx start
        expect "*not initialized"
        send "\n"
        exit
END
    fi
    if [ -f ~/.config/fcitx/profile ]; then
      sed -i "s/pinyin-libpinyin:False/pinyin-libpinyin:True/" ~/.config/fcitx/profile
      cp ~/.config/fcitx/profile /home/HwHiAiUser/.config/fcitx/profile
      chown -R HwHiAiUser /home/HwHiAiUser/.config/fcitx/profile
    fi
  fi
  log "Finish building local desktop function."
}

# 远程图形化界面安装函数
add_remote_desktop() {
  log "Begin to build remote desktop function."
  # 配置firefox下载渠道
  if [ ! -f /etc/apt/sources.list.d/mozillateam-ubuntu-ppa-jammy.list ]; then
    log "Modify firefox downloading source."
    if [ "$FLAG_VERBOSE" == true ]; then
      sysctl net.ipv6.conf.all.disable_ipv6=1
      add-apt-repository ppa:mozillateam/ppa -y
    else
      sysctl net.ipv6.conf.all.disable_ipv6=1 > /dev/null 2>&1
      add-apt-repository ppa:mozillateam/ppa -y > /dev/null 2>&1
    fi
    # prioritize the apt version of firefox over the snap version
    printf "Package: *
Pin: release o=LP-PPA-mozillateam
Pin-Priority: 1001

Package: firefox
Pin: version 1:1snap1-0ubuntu2
Pin-Priority: -1
" > /etc/apt/preferences.d/mozilla-firefox
    # 配置launchpad反向代理
    printf "deb https://launchpad.proxy.ustclug.org/mozillateam/ppa/ubuntu/ jammy main" > /etc/apt/sources.list.d/mozillateam-ubuntu-ppa-jammy.list
  fi

  log "Download remote desktop apt dependencies."
  apt_remote_desktop_required=("network-manager-gnome" "xfce4" "xfce4-goodies" "tightvncserver" "firefox" "fcitx" "fcitx-libpinyin" "language-pack-zh-hans")
  apt_remote_desktop_installed=()
  apt_remote_desktop_to_install=()
  for package in "${apt_remote_desktop_required[@]}"; do
    if [ "$(dpkg -l | grep "$package")" != "" ] && [ "$package" != "xfce4" ]; then
      apt_remote_desktop_installed+=("$package")
    else
      apt_remote_desktop_to_install+=("$package")
    fi
  done
  mapfile -t apt_remote_desktop_installed < <(for package in "${apt_remote_desktop_installed[@]}"; do echo "${package}"; done | sort -u)
  mapfile -t apt_remote_desktop_to_install < <(for package in "${apt_remote_desktop_to_install[@]}"; do echo "${package}"; done | sort -u)
  apt_remote_desktop_installed_str="These packages have been installed:"
  for package in "${apt_remote_desktop_installed[@]}"; do
    apt_remote_desktop_installed_str+=" $package"
  done
  if [ ${#apt_remote_desktop_installed[@]} -ne 0 ]; then log "$apt_remote_desktop_installed_str."; fi
  if [ ${#apt_remote_desktop_to_install[@]} -ne 0 ]; then
    apt_remote_desktop_to_install_log_str="These packages are to install:"
    apt_remote_desktop_to_install_str="apt-get -f install"
    for package in "${apt_remote_desktop_to_install[@]}"; do
      apt_remote_desktop_to_install_log_str+=" $package"
      apt_remote_desktop_to_install_str+=" $package"
    done
    apt_remote_desktop_to_install_str+=" -y"
    log "$apt_remote_desktop_to_install_log_str."
    if [ "$FLAG_VERBOSE" == true ]; then
        apt-get update
        $apt_remote_desktop_to_install_str
    else
        apt-get update > /dev/null 2>&1
        $apt_remote_desktop_to_install_str > /dev/null 2>&1
    fi
  else
    log "No packages to install."
  fi

  # 配置vnc server启动环境
  log "Create and turn on vnc server service."
  # 文件路径
  mkdir -p "$HOME"/.vnc
  # 创建并写入文件内容
  echo "#!/bin/bash
startxfce4 &" > "$HOME/.vnc/xstartup"
  chmod +x "$HOME/.vnc/xstartup"
  # vnc 自启动
  echo "[Unit]
Description=Start TightVNC server at startup
After=multi-user.target

[Service]
Type=forking
User=root
ExecStart=/bin/sh -c '/usr/bin/vncserver -depth 24 -geometry 1920x1080 :%i'
ExecStop=/usr/bin/vncserver -kill :%i

[Install]
WantedBy=default.target" > /etc/systemd/system/vncserver@.service
  systemctl daemon-reload
  if [ "$FLAG_VERBOSE" == true ]; then
    if [ "$(systemctl is-enabled vncserver@1.service)" != "enabled" ]; then
      systemctl enable vncserver@1.service
    fi
  else
    if [ "$(systemctl is-enabled vncserver@1.service)" != "enabled" ]; then
      systemctl enable vncserver@1.service > /dev/null 2>&1
    fi
  fi

  # 重新安装netplan
  log "Reinstall netplan."
  if [ "$FLAG_VERBOSE" == true ]; then
    if [ "$(dpkg -l | grep netplan.io)" == "" ]; then
      apt-get install netplan.io -y
    fi
  else
    if [ "$(dpkg -l | grep netplan.io)" == "" ]; then
      apt-get install netplan.io -y > /dev/null 2>&1
    fi
  fi

  # 提示修改密码时设置vnc server
  log "Set password for vnc server"
  if [ "$(echo ls "$HOME"/.vnc | grep davinci-mini:1)" == "" ]; then
    if [ "$FLAG_VERBOSE" == true ]; then
      expect <<- END
        spawn vncserver
        expect "Password:*"
        send "Mind@123\n"
        expect "Verify:*"
        send "Mind@123\n"
        expect "Would you like to enter a view-only password (y/n)?*"
        send "n\n"
        exit
END
    else
      expect <<- END > /dev/null 2>&1
        spawn vncserver
        expect "Password:*"
        send "Mind@123\n"
        expect "Verify:*"
        send "Mind@123\n"
        expect "Would you like to enter a view-only password (y/n)?*"
        send "n\n"
        exit
END
    fi
  fi
  log "Set up fcitx."
  # 设置中文输入法自启动，本地桌面的输入法自动自启
  mkdir -p /root/.config/autostart
  if [ ! -e /root/.config/autostart/fcitx-autostart.desktop ]; then
    cp /usr/share/fcitx/xdg/autostart/fcitx-autostart.desktop /root/.config/autostart
  fi
  # 设置fcitx为默认输入法
  if [ "$(im-config -m | grep fcitx)" == "" ]; then
      im-config -n fcitx
  fi
  # 为root和HwHiAiUser用户添加中文键盘布局
  if [ "$(pgrep -f "fcitx")" == "" ]; then
    if [ "$FLAG_VERBOSE" == true ]; then
      expect <<- END
        spawn fcitx start
        expect "*not initialized"
        send "\n"
        exit
END
    else
      expect <<- END > /dev/null 2>&1
        spawn fcitx start
        expect "*not initialized"
        send "\n"
        exit
END
    fi
    if [ -f ~/.config/fcitx/profile ]; then
      sed -i "s/pinyin-libpinyin:False/pinyin-libpinyin:True/" ~/.config/fcitx/profile
      cp ~/.config/fcitx/profile /home/HwHiAiUser/.config/fcitx/profile
      chown -R HwHiAiUser /home/HwHiAiUser/.config/fcitx/profile
    fi
  fi
  log "Finish building remote desktop function."
}

add_wifi() {
  set -e
  log "Begin to build wifi function."
  log "Begin to install wifi dependencies."
  path_wifi="$path_download"/wifi
  if [ ! -e "$path_wifi" ] || [ -z "$(ls -A "$path_wifi")" ] > /dev/null 2>&1; then
    if find "$path_download"/*wifi*.zip > /dev/null 2>&1; then
      wifi_zip_name=$(find "$path_download"/*wifi*.zip | awk -F'[/]' '{print $NF}')
      over_amount_flag=$(echo "$wifi_zip_name" | awk -F' ' '{print $2}')
      if [ "$over_amount_flag" != "" ]; then
        log "Please provide unique wifi zip package."
        return 1
      fi
    fi
    if [ "$wifi_zip_name" == "" ]; then
      url_wifi="$(jq -r '.source.url.wifi' "$config_path")"
      if [ "$url_wifi" == "" ]; then
        og "Please provide wifi downloading url or wifi zip package."
        return 1
      fi
      wifi_zip_name=$(echo "$url_wifi" | rev | cut -d/ -f1 | rev)
      wifi_zip_backup_name=${wifi_zip_name%.*}\.backup\.${wifi_zip_name##*.}
      if [ "$FLAG_VERBOSE" == true ]; then
        wget "$url_wifi" -O "$path_download"/"$wifi_zip_backup_name" --no-check-certificate
      else
        wget "$url_wifi" -O "$path_download"/"$wifi_zip_backup_name" --no-check-certificate > /dev/null 2>&1
      fi
      mv "$path_download"/"$wifi_zip_backup_name" "$path_download"/"$wifi_zip_name"
    fi
    path_wifi_backup=$path_wifi\.backup
    if [ "$FLAG_VERBOSE" == true ]; then
      unzip "$path_download"/"$wifi_zip_name" -d "$path_wifi_backup"
    else
      unzip "$path_download"/"$wifi_zip_name" -d "$path_wifi_backup" > /dev/null 2>&1
    fi
    mv "$path_wifi_backup" "$path_wifi"
  fi

  # 设置USB WiFi驱动自动加载
  log "Set usb wifi auto loaded."
  mkdir -p /usr/lib/firmware/rtlwifi/
  cp "$path_wifi"/*.bin /usr/lib/firmware/rtlwifi/
  cp "$path_wifi"/*.ko /lib/modules/5.10.0+/
  printf "rfkill
cfg80211
mac80211
rtlwifi
rtl_usb
rtl8192c-common
rtl8192cu" > /etc/modules-load.d/usb-wifi-drivers.conf
  depmod -a

  log "Finish building wifi function."
}

# 后处理函数
post_process() {
  # 解决后处理插件使用报错问题
  log "Copy libpython3.so to /usr/lib64."
  cp "$path_install"/miniconda3/lib/libpython3.9.so.1.0 /usr/lib64
  if [ -f /root/.cache/gstreamer-1.0/registry.aarch64.bin ]; then
      rm /root/.cache/gstreamer-1.0/registry.aarch64.bin
  fi

  # 添加oom_killer
  printf "#\041/bin/bash
chmod 666 /sys/fs/cgroup/memory/usermemory/tasks
echo 1 > /proc/sys/vm/enable_oom_killer
echo 0 > /sys/fs/cgroup/memory/usermemory/memory.oom_control
" > /var/oom_killer.sh
  sudo chmod +x /var/oom_killer.sh
  printf "[Unit]
Description=%s
Wants=start-davinci.service
After=start-davinci.service

[Service]
ExecStart=%s
Restart=on-failure
RestartSec=10s

[Install]
WantedBy=multi-user.target" /var/oom_killer.sh /var/oom_killer.sh > /etc/systemd/system/oom_killer.service
  if [ "$(cat < /root/.bashrc | grep usermemory)" == "" ]; then
    printf "echo \$\$ > /sys/fs/cgroup/memory/usermemory/tasks" >> /root/.bashrc
  fi
  if [ "$(cat < /home/HwHiAiUser/.bashrc | grep usermemory)" == "" ]; then
    printf "echo \$\$ > /sys/fs/cgroup/memory/usermemory/tasks" >> /home/HwHiAiUser/.bashrc
  fi
  systemctl daemon-reload
  if [ "$FLAG_VERBOSE" == true ]; then
    systemctl enable oom_killer
  else
    systemctl enable oom_killer > /dev/null 2>&1
  fi

  # 修改启动显示文本
  log "Change startup text."
  if [ -e /usr/games/lolcat ] && [ ! -e /usr/bin/lolcat ]; then
    mv /usr/games/lolcat /usr/bin
  fi
	rm /etc/update-motd.d/*
  if [ ! -e /etc/update-motd.d/01-ascend-devkit-startup-text ]; then
      printf "#\041/bin/sh
figlet -tk -w 120 \"Ascend-devkit\" | lolcat -f -S 22
printf \"Welcome to Atlas 200I DK A2\\\n\"
[ -r /etc/lsb-release ] && . /etc/lsb-release

if [ -z \"\$DISTRIB_DESCRIPTION\" ] && [ -x /usr/bin/lsb_release ]; then
        # Fall back to using the very slow lsb_release utility
        DISTRIB_DESCRIPTION=\$(lsb_release -s -d)
fi
printf \"This system is based on %%s (%%s %%s %%s)\\\n\" \"\$DISTRIB_DESCRIPTION\" \"\$(uname -o)\" \"\$(uname -r)\" \"\$(uname -m)\"
printf \"\\\n\"
printf \"This system is only applicable to individual developers and cannot be used for commercial purposes.\\\n\"
printf \"\\\n\"
printf \"By using this system, you have agreed to the Huawei Software License Agreement.\\\n\"
printf \"Please refer to the agreement for details on https://www.hiascend.com/software/protocol\\\n\"
printf \"\\\n\"
printf \"Reference resources\\\n\"
printf \"* Home page: https://www.hiascend.com/hardware/developer-kit-a2\\\n\"
printf \"* Documentation: https://www.hiascend.com/hardware/developer-kit-a2/resource\\\n\"
printf \"* Online courses: https://www.hiascend.com/edu/courses\\\n\"
printf \"* Online experiments: https://www.hiascend.com/zh/edu/experiment\\\n\"
printf \"* Forum: https://www.hiascend.com/forum/\\\n\"
printf \"* Code: https://gitee.com/HUAWEI-ASCEND/ascend-devkit\\\n\"
printf \"\\\n\"" > /etc/update-motd.d/01-ascend-devkit-startup-text
  fi
  chmod +x /etc/update-motd.d/01-ascend-devkit-startup-text
  # 再次修改时区
  if [ "$FLAG_VERBOSE" == true ]; then
    timedatectl set-timezone Asia/Shanghai
  else
    timedatectl set-timezone Asia/Shanghai > /dev/null 2>&1
  fi
}

# 定制化处理函数
accident_handler() {
  cleanup_temporary_files "$path_download"/"$miniconda_file_backup_name"
  cleanup_temporary_files "$path_download"/"$acl_runtime_file_backup_name"
  cleanup_temporary_files "$path_download"/"$ais_bench_file_backup_name"
  cleanup_temporary_files "$path_download"/"$cann_file_backup_name"
  cleanup_temporary_files "$path_download"/"$mxvision_file_backup_name"
  cleanup_temporary_files "$path_download"/"$audio_zip_backup_name"
  cleanup_temporary_files "$path_download"/"$graphic_zip_backup_name"
  cleanup_temporary_files "$path_download"/"$wifi_zip_backup_name"
  cleanup_temporary_files "$path_download"
}

#!/usr/bin/env bash

pre_process() {
  set -e
  log "Begin to conduct pre-process."
  sed -i '/export LD_LIBRARY_PATH=\/home\/HwHiAiUser\/Ascend\/acllib\/lib64/d' /home/HwHiAiUser/.bashrc
}

dnf_install() {
  set -e
  log "Begin to install dnf dependencies."
  # 构建dnf将要下载的数组
  dnf_required=()
  # 镜像压缩和扩容所需依赖
  dnf_required+=("dosfstools" "dos2unix" "expect" "parted" "dump")
  # 系统所需依赖
  dnf_required+=("git" "gcc" "g++" "make" "wget" "yum")
  # 视频解析以及外设等依赖
  dnf_required+=("ffmpeg" "v4l-utils")
  # Wi-Fi所需依赖
  dnf_required+=("NetworkManager-wifi")
  # psutil 所需依赖
  dnf_required+=("python3-devel")
  mapfile -t dnf_required < <(for package in "${dnf_required[@]}"; do echo "${package}"; done | sort -u)
  dnf_required_log_str="These packages are to install:"
  dnf_required_str="dnf install"
  for package in "${dnf_required[@]}"; do
    dnf_required_log_str+=" $package"
    dnf_required_str+=" $package"
  done
  dnf_required_str+=" -y"
  log "$dnf_required_log_str."
  if [ "$FLAG_VERBOSE" == true ]; then
    dnf update
    $dnf_required_str
  else
    dnf update > /dev/null 2>&1
    $dnf_required_str > /dev/null 2>&1
  fi
  log "Finish installing dnf dependencies."
}

python_pip_install() {
  set -e
  log "Begin to install python dependencies."

  # 创建python软链接
  ln -s /usr/bin/python3.9 /usr/bin/python
  chmod 777 /usr/bin/python
  # 若不存在，则新建~/.pip目录
  mkdir -p ~/.pip
  # 配置pip下载源为豆瓣源
  log "Modify pip downloading sources."
  pip_source_cfg=$(jq -r '.python.source' "$config_path")
  if [ "$pip_source_cfg" != "" ]; then
    printf "%s" "$pip_source_cfg" | sed "s/'\|\"//g" > ~/.pip/pip.conf
  fi

  # pip 第三方依赖
  pip_dependencies_array=("torch==1.13.0" "torchaudio==0.13.0" "torchvision==0.14.0" "scikit-video" "ipywidgets" "ffmpeg-python" "IPython==7.34.0" "numpy==1.22.4" "albumentations" "jupyterlab" "matplotlib" "pycocotools" "protobuf==3.20.0")
  # CANN pip依赖
  pip_dependencies_array+=("attrs" "numpy" "decorator" "cffi" "pyyaml" "pathlib2" "psutil" "protobuf" "scipy" "requests" "absl-py")
  # atc python依赖
  pip_dependencies_array+=("sympy")
  mapfile -t pip_install_array < <(for package in "${pip_dependencies_array[@]}"; do echo "${package}"; done | sort -u)
  pip_str="pip install --no-input"
  for package in "${pip_install_array[@]}"; do
    pip_str+=" $package"
  done
  # acl-runtime 安装包名称
  acl_runtime_url="$(jq -r '.source.url["acl-runtime"]' "$config_path")"
  acl_runtime_file_name=$(echo "$acl_runtime_url" | rev | cut -d/ -f1 | rev)
  acl_runtime_file_backup_name=${acl_runtime_file_name%.*}\.backup\.${acl_runtime_file_name##*.}
  # ais_bench 安装包名称
  ais_bench_url="$(jq -r '.source.url.ais_bench' "$config_path")"
  ais_bench_file_name=$(echo "$ais_bench_url" | rev | cut -d/ -f1 | rev)
  ais_bench_file_backup_name=${ais_bench_file_name%.*}\.backup\.${ais_bench_file_name##*.}
  # shellcheck disable=SC2001
  pip_log_str=$(echo "$pip_str" | sed 's/^\(.\)/\U\1/')
  pip_log_str+=" python-miio acl-runtime ais_bench"

  if [ "$FLAG_VERBOSE" == true ]; then
    log "$pip_log_str."
    $pip_str
    # 使用豆瓣源下载python-miio，绕过ssl认证
    pip install git+https://github.com/rytilahti/python-miio.git
    # 下载aclruntime和ais_bench的wheel安装包
    if [ ! -f "$path_download"/"$acl_runtime_file_name" ]; then
      wget "$acl_runtime_url" -O "$path_download"/"$acl_runtime_file_backup_name" --no-check-certificate
      mv "$path_download"/"$acl_runtime_file_backup_name" "$path_download"/"$acl_runtime_file_name"
    fi
    pip install "$path_download"/"$acl_runtime_file_name" --no-input
    if [ ! -f "$path_download"/"$ais_bench_file_name" ]; then
      wget "$ais_bench_url" -O "$path_download"/"$ais_bench_file_backup_name" --no-check-certificate
      mv "$path_download"/"$ais_bench_file_backup_name" "$path_download"/"$ais_bench_file_name"
    fi
    pip install "$path_download"/"$ais_bench_file_name" --no-input
  else
    log "$pip_log_str."
    $pip_str > /dev/null 2>&1
    # 使用豆瓣源下载python-miio，绕过ssl认证
    pip install git+https://github.com/rytilahti/python-miio.git > /dev/null 2>&1
    # 下载aclruntime和ais_bench的wheel安装包
    if [ ! -f "$path_download"/"$acl_runtime_file_name" ]; then
      wget "$acl_runtime_url" -O "$path_download"/"$acl_runtime_file_backup_name" --no-check-certificate > /dev/null 2>&1
      mv "$path_download"/"$acl_runtime_file_backup_name" "$path_download"/"$acl_runtime_file_name"
    fi
    pip install "$path_download"/"$acl_runtime_file_name" --no-input > /dev/null 2>&1
    if [ ! -f "$path_download"/"$ais_bench_file_name" ]; then
      wget "$ais_bench_url" -O "$path_download"/"$ais_bench_file_backup_name" --no-check-certificate > /dev/null 2>&1
      mv "$path_download"/"$ais_bench_file_backup_name" "$path_download"/"$ais_bench_file_name"
    fi
    pip install "$path_download"/"$ais_bench_file_name" --no-input > /dev/null 2>&1
  fi
  log "Finish installing python dependencies."
}

install_cann() {
  log "Begin to add CANN."
  # 下载并安装CANN
  log "Download CANN."
  if find "$path_download"/*cann* > /dev/null 2>&1; then
    cann_file_name=$(find "$path_download"/*cann* | awk -F'[/]' '{print $NF}')
    over_amount_flag=$(echo "$cann_file_name" | awk -F' ' '{print $2}')
    if [ "$over_amount_flag" != "" ]; then
      log "Please provide unique cann install scripts."
      return 1
    fi
  fi
  if [ "$cann_file_name" == "" ]; then
    url_cann="$(jq -r '.source.url.cann' "$config_path")"
    if [ "$url_cann" == "" ]; then
      log "Please provide cann downloading url or cann install scripts."
      return 1
    fi
    cann_file_name=$(echo "$url_cann" | rev | cut -d/ -f1 | rev)
    cann_file_backup_name=${cann_file_name%.*}\.backup\.${cann_file_name##*.}
    if [ "$FLAG_VERBOSE" == true ]; then
      wget "$url_cann" -O "$path_download"/"$cann_file_backup_name" --no-check-certificate
      mv "$path_download"/"$cann_file_backup_name" "$path_download"/"$cann_file_name"
    else
      wget "$url_cann" -O "$path_download"/"$cann_file_backup_name" --no-check-certificate > /dev/null 2>&1
      mv "$path_download"/"$cann_file_backup_name" "$path_download"/"$cann_file_name" > /dev/null 2>&1
    fi
  fi
  log "Install CANN."
  chmod +x "$path_download"/"$cann_file_name"
  if [ "$FLAG_VERBOSE" == true ]; then
    "$path_download"/"$cann_file_name" --full
  else
    "$path_download"/"$cann_file_name" --full > /dev/null 2>&1
  fi
  if [ ! -e /root/.bashrc ]; then
    touch /root/.bashrc
  fi
  if [ "$(cat < /etc/profile.d/ascend-devkit.sh | grep ascend-toolkit)" == "" ]; then
    printf "source %s/Ascend/ascend-toolkit/set_env.sh" "$path_install" >> /etc/profile.d/ascend-devkit.sh
  fi
  log "Finish adding CANN."
}

install_mxvision() {
  log "Begin to add mxVision."
  # 下载并安装mxvision
  log "Download mxVision."
  if [ -f "$path_install"/Ascend/ascend-toolkit/set_env.sh ]; then
    source "$path_install"/Ascend/ascend-toolkit/set_env.sh
  fi
  if find "$path_download"/*mxvision* > /dev/null 2>&1; then
    mxvision_file_name=$(find "$path_download"/*mxvision* | awk -F'[/]' '{print $NF}')
    over_amount_flag=$(echo "$mxvision_file_name" | awk -F' ' '{print $2}')
    if [ "$over_amount_flag" != "" ]; then
      log "Please provide unique mxVision install scripts."
      return 1
    fi
  fi
  if [ "$mxvision_file_name" == "" ]; then
    url_mxvision="$(jq -r '.source.url.mxvision' "$config_path")"
    if [ "$url_mxvision" == "" ]; then
      log "Please provide mxVision downloading url or mxvision install scripts."
      return 1
    fi
    mxvision_file_name=$(echo "$url_mxvision" | rev | cut -d/ -f1 | rev)
    mxvision_file_backup_name=${mxvision_file_name%.*}\.backup\.${mxvision_file_name##*.}
    if [ "$FLAG_VERBOSE" == true ]; then
      wget "$url_mxvision" -O "$path_download"/"$mxvision_file_backup_name" --no-check-certificate
    else
      wget "$url_mxvision" -O "$path_download"/"$mxvision_file_backup_name" --no-check-certificate > /dev/null 2>&1
    fi
    mv "$path_download"/"$mxvision_file_backup_name" "$path_download"/"$mxvision_file_name"
  fi
  chmod +x "$path_download"/"$mxvision_file_name"
  log "Install mxVision."
  if [ "$FLAG_VERBOSE" == true ]; then
    "$path_download"/"$mxvision_file_name" --install --install-path=/root/Ascend
  else
    "$path_download"/"$mxvision_file_name" --install --install-path=/root/Ascend > /dev/null 2>&1
  fi
  log "Finish adding mxVision."
}

install_acllite() {
  set -e
  log "Begin to add acllite."

  # 下载并安装acllite
  log "Download acllite."
  path_acllite=$path_install/Ascend/thirdpart/aarch64
  mkdir -p "$path_acllite"
  if [ ! -e "$path_acllite"/acllite ]; then
    if [ ! -e "$path_download"/samples ]; then
      if [ "$FLAG_VERBOSE" == true ]; then
        git clone -b v0.8.0 https://gitee.com/ascend/samples.git "$path_download"/samples
      else
        git clone -b v0.8.0 https://gitee.com/ascend/samples.git "$path_download"/samples > /dev/null 2>&1
      fi
    fi
    log "Copy acllite to ascend path."
    cp -r "$path_download"/samples/python/common/acllite "$path_acllite"/acllite
  fi
  log "Finish adding acllite."
}

add_local_desktop() {
  set -e
  log "Begin to build local desktop function."
  # 安装所需依赖
  log "Begin to install dnf dependencies."
  # 构建dnf将要下载的数组
  dnf_required=()
  # 安装字库
  dnf_required+=("dejavu-fonts" "liberation-fonts" "gnu-*-fonts" "google-*-fonts")
  # 安装Xorg
  dnf_required+=("xorg-*")
  # 安装XFCE及组件
  dnf_required+=("xfwm4" "xfdesktop" "xfce4-*" "xfce4-*-plugin" "network-manager-applet" "*fonts")
  # 安装登录管理器
  dnf_required+=("lightdm" "lightdm-gtk")
  # 安装火狐
  dnf_required+=("firefox")
  # 安装中文输入法
  dnf_required+=("fcitx" "fcitx-qt5" "fcitx-cloudpinyin" "fcitx-sunpinyin" "fcitx-configtool")
  mapfile -t dnf_required < <(for package in "${dnf_required[@]}"; do echo "${package}"; done | sort -u)
  dnf_required_log_str="These packages are to install:"
  dnf_required_str="dnf install"
  for package in "${dnf_required[@]}"; do
    dnf_required_log_str+=" $package"
    dnf_required_str+=" $package"
  done
  dnf_required_str+=" -y"
  log "$dnf_required_log_str."
  if [ "$FLAG_VERBOSE" == true ]; then
    dnf update -y
    $dnf_required_str
  else
    dnf update -y > /dev/null 2>&1
    $dnf_required_str > /dev/null 2>&1
  fi

  # 更新lib和ko
  log "Download graphic libs and kos."
  path_graphic="$path_download"/graphic
  if [ ! -e "$path_graphic" ] || [ -z "$(ls -A "$path_graphic")" ] > /dev/null 2>&1; then
    if find "$path_download"/*graphic*.zip; then
      graphic_zip_name=$(find "$path_download"/*graphic*.zip | awk -F'[/]' '{print $NF}')
      over_amount_flag=$(echo "$graphic_zip_name" | awk -F' ' '{print $2}')
      if [ "$over_amount_flag" != "" ]; then
        log "Please provide unique graphic zip package."
        return 1
      fi
    fi
    if [ "$graphic_zip_name" == "" ]; then
      url_graphic="$(jq -r '.source.url.graphic' "$config_path")"
      if [ "$url_graphic" == "" ]; then
        og "Please provide graphic downloading url or graphic zip package."
        return 1
      fi
      graphic_zip_name=$(echo "$url_graphic" | rev | cut -d/ -f1 | rev)
      graphic_zip_backup_name=${graphic_zip_name%.*}\.backup\.${graphic_zip_name##*.}
      if [ "$FLAG_VERBOSE" == true ]; then
        wget "$url_graphic" -O "$path_download"/"$graphic_zip_backup_name" --no-check-certificate
      else
        wget "$url_graphic" -O "$path_download"/"$graphic_zip_backup_name" --no-check-certificate > /dev/null 2>&1
      fi
      mv "$path_download"/"$graphic_zip_backup_name" "$path_download"/"$graphic_zip_name"
    fi
    path_graphic_backup=$path_graphic\.backup
    if [ "$FLAG_VERBOSE" == true ]; then
      unzip "$path_download"/"$graphic_zip_name" -d "$path_graphic_backup"
    else
      unzip "$path_download"/"$graphic_zip_name" -d "$path_graphic_backup" > /dev/null 2>&1
    fi
    mv "$path_graphic_backup" "$path_graphic"
  fi

  # 更换dtb
  log "Update dtb."
  if [ "$FLAG_VERBOSE" == true ]; then
    dd if="$path_graphic"/dt.img of=/dev/mmcblk1 count=4096 seek=114688 bs=512
    dd if="$path_graphic"/dt.img of=/dev/mmcblk1 count=4096 seek=376832 bs=512
  else
    dd if="$path_graphic"/dt.img of=/dev/mmcblk1 count=4096 seek=114688 bs=512 > /dev/null 2>&1
    dd if="$path_graphic"/dt.img of=/dev/mmcblk1 count=4096 seek=376832 bs=512 > /dev/null 2>&1
  fi

  # 复制ko到相应路径
  if [ ! -f /lib/modules/5.10.0+/ascend_vdp_drm.ko  ]; then
    cp "$path_graphic"/ascend_vdp_drm.ko /lib/modules/5.10.0+/
  fi
  if [ "$(lsmod | grep ascend_vdp_drm)" == "" ]; then
      insmod /lib/modules/5.10.0+/ascend_vdp_drm.ko
  fi

  # 配置本地桌面服务，设置图形桌面驱动自动加载
  log "Create and turn on local desktop service."
  printf "#\041/bin/bash
insmod /lib/modules/5.10.0+/ascend_vdp_drm.ko
nmcli radio wifi on
systemctl restart display-manager.service
" > /usr/local/bin/toggle_graphical_desktop.sh
  sudo chmod +x /usr/local/bin/toggle_graphical_desktop.sh
  printf "[Unit]
Description=%s
Wants=start-davinci.service
After=start-davinci.service

[Service]
ExecStart=%s
Restart=on-failure
RestartSec=10s

[Install]
WantedBy=multi-user.target" /usr/local/bin/toggle_graphical_desktop.sh /usr/local/bin/toggle_graphical_desktop.sh > /etc/systemd/system/toggle_graphical_desktop.service
  systemctl daemon-reload
  # 设置默认桌面为XFCE 通过root权限用户设置
  echo 'user-session=xfce' >> /etc/lightdm/lightdm.conf.d/60-lightdm-gtk-greeter.conf
  if [ "$FLAG_VERBOSE" == true ]; then
    # 设置桌面服务自启动
    systemctl enable toggle_graphical_desktop
    # 设置开机自启动图形界面
    systemctl enable lightdm
    # 设置系统默认以图形界面登录
    systemctl set-default graphical.target
  else
    # 设置桌面服务自启动
    systemctl enable toggle_graphical_desktop > /dev/null 2>&1
    # 设置开机自启动图形界面
    systemctl enable lightdm > /dev/null 2>&1
    # 设置系统默认以图形界面登录
    systemctl set-default graphical.target > /dev/null 2>&1
  fi
}

add_wifi() {
  set -e
  log "Begin to build wifi function."
  log "Begin to install wifi dependencies."
  path_wifi="$path_download"/wifi
    if [ ! -e "$path_wifi" ] || [ -z "$(ls -A "$path_wifi")" ] > /dev/null 2>&1; then
    if find "$path_download"/*wifi*.zip > /dev/null 2>&1; then
      wifi_zip_name=$(find "$path_download"/*wifi*.zip | awk -F'[/]' '{print $NF}')
      over_amount_flag=$(echo "$wifi_zip_name" | awk -F' ' '{print $2}')
      if [ "$over_amount_flag" != "" ]; then
        log "Please provide unique wifi zip package."
        return 1
      fi
    fi
    if [ "$wifi_zip_name" == "" ]; then
      url_wifi="$(jq -r '.source.url.wifi' "$config_path")"
      if [ "$url_wifi" == "" ]; then
        og "Please provide wifi downloading url or wifi zip package."
        return 1
      fi
      wifi_zip_name=$(echo "$url_wifi" | rev | cut -d/ -f1 | rev)
      wifi_zip_backup_name=${wifi_zip_name%.*}\.backup\.${wifi_zip_name##*.}
      if [ "$FLAG_VERBOSE" == true ]; then
        wget "$url_wifi" -O "$path_download"/"$wifi_zip_backup_name" --no-check-certificate
      else
        wget "$url_wifi" -O "$path_download"/"$wifi_zip_backup_name" --no-check-certificate > /dev/null 2>&1
      fi
      mv "$path_download"/"$wifi_zip_backup_name" "$path_download"/"$wifi_zip_name"
    fi
    path_wifi_backup=$path_wifi\.backup
    if [ "$FLAG_VERBOSE" == true ]; then
      unzip "$path_download"/"$wifi_zip_name" -d "$path_wifi_backup"
    else
      unzip "$path_download"/"$wifi_zip_name" -d "$path_wifi_backup" > /dev/null 2>&1
    fi
    mv "$path_wifi_backup" "$path_wifi"
  fi

  # 设置USB WiFi驱动自动加载
  log "Set usb wifi auto loaded."
  mkdir -p /usr/lib/firmware/rtlwifi/
  cp "$path_wifi"/*.bin /usr/lib/firmware/rtlwifi/
  cp "$path_wifi"/*.ko /lib/modules/5.10.0+/
  printf "rfkill
cfg80211
mac80211
rtlwifi
rtl_usb
rtl8192c-common
rtl8192cu" > /etc/modules-load.d/usb-wifi-drivers.conf
  depmod -a

  log "Finish building wifi function."
}

post_process() {
  set -e
  log "Begin to conduct post process"
  # 添加oom_killer
  printf "#\041/bin/bash
chmod 666 /sys/fs/cgroup/memory/usermemory/tasks
echo 1 > /proc/sys/vm/enable_oom_killer
echo 0 > /sys/fs/cgroup/memory/usermemory/memory.oom_control
" > /var/oom_killer.sh
  sudo chmod +x /var/oom_killer.sh
  printf "[Unit]
Description=%s
Wants=start-davinci.service
After=start-davinci.service

[Service]
ExecStart=%s
Restart=on-failure
RestartSec=10s

[Install]
WantedBy=multi-user.target" /var/oom_killer.sh /var/oom_killer.sh> /etc/systemd/system/oom_killer.service
  if [ "$(cat < /etc/profile.d/ascend-devkit.sh | grep usermemory)" == "" ]; then
    printf "echo \$\$ > /sys/fs/cgroup/memory/usermemory/tasks" >> /etc/profile.d/ascend-devkit.sh
  fi
  systemctl daemon-reload
  if [ "$FLAG_VERBOSE" == true ]; then
    systemctl enable oom_killer
  else
    systemctl enable oom_killer > /dev/null 2>&1
  fi

  # 修改启动欢迎文本
  printf "Welcome to Atlas 200I DK A2
This system is only applicable to individual developers and cannot be used for commercial purposes.
By using this system, you have agreed to the Huawei Software License Agreement.
Please refer to the agreement for details on https://www.hiascend.com/software/protocol
Reference resources
* Home page: https://www.hiascend.com/hardware/developer-kit-a2
* Documentation: https://www.hiascend.com/hardware/developer-kit-a2/resource
* Online courses: https://www.hiascend.com/edu/courses
* Online experiments: https://www.hiascend.com/zh/edu/experiment
* Forum: https://www.hiascend.com/forum/
* Code: https://gitee.com/HUAWEI-ASCEND/ascend-devkit" > /etc/motd
  log "Post process finished."
}

# 定制化处理函数
accident_handler() {
  cleanup_temporary_files "$path_download"/"$acl_runtime_file_backup_name"
  cleanup_temporary_files "$path_download"/"$ais_bench_file_backup_name"
  cleanup_temporary_files "$path_download"/"$cann_file_backup_name"
  cleanup_temporary_files "$path_download"/"$mxvision_file_backup_name"
  cleanup_temporary_files "$path_download"/"$audio_zip_backup_name"
  cleanup_temporary_files "$path_download"/"$graphic_zip_backup_name"
  cleanup_temporary_files "$path_download"/"$wifi_zip_backup_name"
  cleanup_temporary_files "$path_download"
}
